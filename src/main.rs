mod handlers;
mod models;
mod routes;

use crate::models::{Backend, Scheduler};
use std::sync::Arc;
use tokio::sync::RwLock;
use warp::{
    http,
    http::{Response, StatusCode},
    Filter,
};

#[tokio::main]
async fn main() {
    let scheduler = Arc::new(RwLock::new(Scheduler::new(0)));
    let backend: Backend = Backend::new(scheduler);

    warp::serve(routes::backend_routes(backend))
        .run(([127, 0, 0, 1], 3000))
        .await
}
