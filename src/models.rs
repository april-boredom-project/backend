use std::{boxed::Box, sync::Arc};
use tokio::sync::RwLock;
use tokio::{
    sync::{mpsc, Mutex},
    task,
};

#[derive(Clone)]
pub struct Backend {
    pub scheduler: Arc<RwLock<Scheduler>>,
}

impl Backend {
    pub fn new(scheduler: Arc<RwLock<Scheduler>>) -> Backend {
        Backend { scheduler }
    }
}

type Task = Box<dyn FnOnce() + Send + 'static>;

enum Message {
    NewTask(Task),
    Terminate,
}

pub struct Scheduler {
    pub workers: Vec<Worker>,
    tx: mpsc::Sender<Message>,
    worker_rx: Arc<Mutex<mpsc::Receiver<Message>>>,
}

impl Scheduler {
    pub fn new(size: usize) -> Scheduler {
        let (tx, rx) = mpsc::channel(100);
        let rx = Arc::new(Mutex::new(rx));
        let mut workers = Vec::with_capacity(size);
        for id in 0..size {
            workers.push(Worker::new(id, Arc::clone(&rx)));
        }
        Scheduler {
            workers,
            tx,
            worker_rx: Arc::clone(&rx),
        }
    }

    pub fn add_worker(&mut self) {
        self.workers
            .push(Worker::new(self.workers.len(), Arc::clone(&self.worker_rx)));
    }

    pub async fn execute<T>(&self, t: T)
    where
        T: FnOnce() + Send + 'static,
    {
        println!("scheduler called to execute a task");
        let task = Box::new(t);

        println!("Task sent, who'll receive?");
        self.tx.send(Message::NewTask(task)).await;
    }
}

impl Drop for Scheduler {
    asyncfn drop(&mut self) {
        println!("Sending terminate message to all workers");
        for _ in &self.workers {
            self.tx.send(Message::Terminate)??;
        }

        println!("Shutting down all workers.");

        for worker in &mut self.workers {
            println!("Shutting down worker {}", worker.id);

            // if let Some(task) = worker.task.take() {
            //     task.join();
            // }
        }
    }
}

pub struct Worker {
    id: usize,
    task: Option<task::JoinHandle<()>>,
}

impl Worker {
    fn new(id: usize, rx: Arc<Mutex<mpsc::Receiver<Message>>>) -> Worker {
        println!("Worker created: {}", id);
        let task = tokio::spawn(async move {
            loop {
                println!("inside a loop");
                let message = rx.lock().await.recv().await.unwrap();
                println!("Worker {} got a message", id);
                match message {
                    Message::NewTask(t) => {
                        println!("Worker {} got a job; executing.", id);

                        t();
                    }
                    Message::Terminate => {
                        println!("Worker {} was told to terminate.", id);

                        break;
                    }
                }
            }
        });

        Worker {
            id,
            task: Some(task),
        }
    }
}
